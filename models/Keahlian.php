<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "keahlian".
 *
 * @property int $id
 * @property string $keahlian
 */
class Keahlian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'keahlian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['keahlian'], 'required'],
            [['keahlian'], 'string', 'max' => 110],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keahlian' => 'Keahlian',
        ];
    }
    public function getJurnalIzin()
    {
        return $this->hasOne(JurnalIzin::class, ['id' => 'keahlian']);
    }
}
