<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "guru_piket".
 *
 * @property int $id
 * @property int|null $nip
 * @property string $nama_gurupiket
 * @property string|null $jenkel
 */
class GuruPiket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'guru_piket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nip'], 'integer'],
            [['nama_gurupiket'], 'required'],
            [['jenkel'], 'string'],
            [['nama_gurupiket'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nip' => 'Nip',
            'nama_gurupiket' => 'Nama Gurupiket',
            'jenkel' => 'Jenkel',
        ];
    }
    public function getJurnalIzin()
    {
        return $this->hasOne(JurnalIzin::class, ['id' => 'nama_gurupiket']);
    }
}
