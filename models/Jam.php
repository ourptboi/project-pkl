<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jam".
 *
 * @property int $id
 * @property string $jam_pelajaran
 */
class Jam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jam';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jam_pelajaran'], 'required'],
            [['jam_pelajaran'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jam_pelajaran' => 'Jam Pelajaran',
        ];
    }
    public function getJurnalIzin()
    {
        return $this->hasOne(JurnalIzin::class, ['id' => 'jam_pelajaran']);
    }
}
