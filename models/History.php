<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jurnal_izin".
 *
 * @property int $id
 * @property string $nama
 * @property string $kelas
 * @property string $keahlian
 * @property string $jam_mulai
 * @property string $jam_kembali
 * @property string $keterangan
 * @property string $tlp
 * @property string $pengajar
 * @property string $nama_gurupiket
 * @property string|null $status
 * @property string|null $tanggal
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jurnal_izin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'kelas', 'keahlian', 'jam_mulai', 'jam_kembali', 'keterangan', 'tlp', 'pengajar', 'nama_gurupiket'], 'required'],
            [['kelas', 'status'], 'string'],
            [['tanggal'], 'safe'],
            [['nama', 'jam_mulai', 'jam_kembali', 'keterangan', 'nama_gurupiket'], 'string', 'max' => 255],
            [['keahlian', 'pengajar'], 'string', 'max' => 110],
            [['tlp'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'kelas' => 'Kelas',
            'keahlian' => 'Keahlian',
            'jam_mulai' => 'Jam Mulai',
            'jam_kembali' => 'Jam Kembali',
            'keterangan' => 'Keterangan',
            'tlp' => 'No Telepon',
            'pengajar' => 'Pengajar',
            'nama_gurupiket' => 'Nama Gurupiket',
            'status' => 'Status',
            'tanggal' => 'Tanggal',
        ];
    }
    public function getSekolah()
    {
        return $this->kelas . ' ' . $this->keahlian;
    }
}
