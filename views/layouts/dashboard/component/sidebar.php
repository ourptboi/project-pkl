<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <!-- <i class="fas fa-laugh-wink"></i> -->
        </div>
        <div class="sidebar-brand-text mx-3">Pembayaran <sup>spp</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <ul class="nav">
        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="index.html">
                <i class="fas fa-fw fa-chart-bar"></i>
                <span>Dashboard</span>
            </a>
        </li>

        <!-- Nav Item - Dispensasi -->
        <li class="nav-item">
            <a class="nav-link" href="http://localhost/projectbersama/web/index.php?r=jurnal-izin%2Findex">
                <i class='fas fa-fw fa-book'></i>
                <span>Dispensasi</span>
            </a>
        </li>

        <!-- Nav Item - Kelas -->
        <li class="nav-item">
            <a class="nav-link" href="http://localhost/projectbersama/web/index.php?r=keahlian%2Findex">
                <i class="fas fa-fw fa-school"></i>
                <span>Keahlian</span>
            </a>
        </li>

        <!-- Nav Item - Jam Pelajaran -->
        <li class="nav-item">
            <a class="nav-link" href="http://localhost/projectbersama/web/index.php?r=jam%2Findex">
                <i class="fas fa-fw fa-clock"></i>
                <span>Jam Pelajaran</span>
            </a>
        </li>

        <!-- Nav Item - Guru Piket -->
        <li class="nav-item">
            <a class="nav-link" href="http://localhost/projectbersama/web/index.php?r=guru-piket%2Findex">
                <i class="fas fa-fw fa-chalkboard-teacher"></i>
                <span>Guru Piket</span>
            </a>
        </li>

        <!-- Nav Item - History -->
        <li class="nav-item">
            <a class="nav-link" href="http://localhost/projectbersama/web/index.php?r=history%2Findex">
                <i class="fas fa-fw fa-history"></i>
                <span>History</span>
            </a>
        </li>
        <!-- Nav Item - History -->
        <li class="nav-item">
            <a class="nav-link" href="tables.html">
                <i class="fas fa-fw fa-cog"></i>
                <span>Setting</span>
            </a>
        </li>
    </ul>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>

<style>
    .nav {
        font-weight: bold;
    }
</style>