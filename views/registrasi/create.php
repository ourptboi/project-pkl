<?php

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;

/** @var yii\web\View $this */
/** @var app\models\Registrasi $model */

$this->title = 'Create Account';
$this->params['breadcrumbs'][] = ['label' => 'Registrasi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>


<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body>
    <div class="registrasi-create">
        <h1 style="text-align:center"><?= Html::encode($this->title) ?></h1>
        
        <div class="row justify-content-center">
    <div class="col-lg-4 p-4 custom-border">
        <?php $form = ActiveForm::begin([
            'id' => 'registration-form',
            'fieldConfig' => [
                'template' => "{label}\n{input}\n{error}",
                'labelOptions' => ['class' => 'col-lg-1 col-form-label'],
                'inputOptions' => ['class' => 'col-lg-3 form-control'],
                'errorOptions' => ['class' => 'col-lg-7 invalid-feedback'],
            ],
        ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Username') ?>

        <?= $form->field($model, 'password')->passwordInput()->label('Password') ?>

        <?= $form->field($model, 'role', ['options' => ['style' => 'display: none;']])->textInput()->label('Role') ?>

        <div class="form-group">
            <div>
                <?= Html::submitButton('Register', ['class' => 'btn btn-primary form-control', 'name' => 'register-button']) ?>
            </div>
        </div>

        <a href="<?= Yii::$app->urlManager->createUrl(['/site/login']) ?>"><p class="text-center pt-2">Already have an account? Login now!</p></a>

        <style>
            a {
                text-decoration: none;
                font-size: 13px;
            }
        </style>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<style>
    body {
        margin: 0;
        padding: 0;
        background-color: #f6f6f6;
    }

    .registrasi-create {
        font-family: "Poppins", sans-serif;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
    }
  
    .col-lg-4 {
        background-color: #fff;
        border-radius: 8px;
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
    }
  
    h1 {
        text-align: center;
        margin-bottom: 20px;
    }

    .invalid-feedback {
        font-size: 13px; /* Ukuran teks pesan kesalahan */
    }
</style>
