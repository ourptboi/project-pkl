<?php

use app\models\JurnalIzin;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'List Data Izin');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Keahlian'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="row">
                <div class="col-md-15">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'tableOptions' => ['class' => 'table table-bordered'],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            // 'id',
                            'nama',
                            // 'kelas',
                            // 'keahlian',
                            [
                                'attribute' => 'sekolah',
                                'value' => 'Sekolah',
                            ],
                            'jam_mulai',
                            // 'jam_kembali',
                            // 'keterangan',
                            // 'tlp',
                            // 'pengajar',
                            'nama_gurupiket',
                            //'tanggal',
                            [
                                'attribute' => 'status',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    $statusLabel = '';
                                    $statusClass = '';
                            
                                    // Tentukan label dan kelas CSS sesuai dengan nilai status
                                    if ($model->status === 'setuju') {
                                        $statusLabel = 'Setuju';
                                        $statusClass = 'btn btn-success';
                            
                                        // URL WhatsApp dengan nomor yang sesuai
                                        $whatsappUrl = 'https://api.whatsapp.com/send?phone=+6289619218341&text= Permintaan izin keluar sekolah Anda telah berhasil disetujui. Anda dapat segera meninggalkan sekolah sesuai dengan jadwal yang telah Anda tentukan. Harap pastikan untuk kembali sesuai dengan waktu yang telah ditentukan dan beri tahu guru piket jika ada perubahan dalam rencana Anda. Terima kasih!';
                            
                                        return Html::a($statusLabel, $whatsappUrl, ['class' => $statusClass]);
                                    } elseif ($model->status === 'ditolak') {
                                        $statusLabel = 'Ditolak';
                                        $statusClass = 'btn btn-danger';
                                    } elseif ($model->status === 'belum diproses') {
                                        $statusLabel = 'Belum Diproses';
                                        $statusClass = 'btn btn-warning';
                                    }
                            
                                    // Jika status bukan "Setuju", tampilkan tombol dengan label dan kelas CSS yang sesuai
                                    return Html::a($statusLabel, 'javascript:void(0);', ['class' => $statusClass, 'disabled' => true]);
                                    
                                },
                            ],
                            
                            [
                                'class' => ActionColumn::className(),
                                'header' => 'Aksi',
                                'urlCreator' => function ($action, JurnalIzin $model, $key, $index, $column) {
                                    return Url::toRoute([$action, 'id' => $model->id]);
                                }
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>



<?php foreach ($dataProvider->models as $model): ?>

<?php endforeach; ?>


