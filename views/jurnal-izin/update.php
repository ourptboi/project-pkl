<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\JurnalIzin $model */

$this->title = Yii::t('app', 'Update Jurnal Izin: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jurnal Izins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="jurnal-izin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listKeahlian' => $listKeahlian,
        'listJam' => $listJam,
        'listGuruPiket' => $listGuruPiket,
    ]) ?>

</div>
