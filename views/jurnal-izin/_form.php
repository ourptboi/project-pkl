<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\JurnalIzin $model */
/** @var yii\widgets\ActiveForm $form */

$role = Yii::$app->user->isGuest ? null : User::me()->role;

?>

<div class="jurnal-izin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kelas')->dropDownList(['X' => 'X', 'XI' => 'XI', 'XII' => 'XII',], ['prompt' => 'Pilih Kelas']) ?>

    <?= $form->field($model, 'keahlian')->dropDownList(
        ArrayHelper::map($listKeahlian, 'keahlian', 'keahlian'),
        ['prompt' => 'Pilih Jurusan']
    ) ?>

    <?= $form->field($model, 'jam_mulai')->dropDownList(
        ArrayHelper::map($listJam, 'id', 'jam_pelajaran'),
        ['prompt' => 'Pilih Jam Mulai']
    ) ?>

    <?= $form->field($model, 'jam_kembali')->dropDownList(
        ArrayHelper::map($listJam, 'id', 'jam_pelajaran'),
        ['prompt' => 'Pilih Jam Kembali']
    ) ?>

    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tlp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pengajar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_gurupiket')->dropDownList(
        ArrayHelper::map($listGuruPiket, 'id', 'nama_gurupiket'),
        ['prompt' => 'Pilih Guru Piket', 'label' => 'Nama Guru Piket']
    ) ?>

    <?php
    // Hanya tampilkan bidang 'status' jika peran pengguna adalah 'admin' atau 'petugas'
    if ($role === 'admin' || $role === 'petugas') {
        echo $form->field($model, 'status')->dropDownList(['setuju' => 'Setuju', 'ditolak' => 'Ditolak', 'belum diproses' => 'Belum diproses',], ['prompt' => '']);
    }
    ?>

    <?= $form->field($model, 'tanggal')->input('date') ?>


    <script>
        $(document).ready(function() {
            // Set nilai 'tunda' pada input 'status'
            $('#<?= Html::getInputId($model, 'status') ?>').val('belum diproses');
        });
    </script>

    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>