<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\JurnalIzin $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jurnal Izins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="jurnal-izin-view">

    <?php if (Yii::$app->session->hasFlash('success')): ?>
    <?php endif; ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nama',
            'kelas',
            'keahlian',
            'jam_mulai',
            'jam_kembali',
            'keterangan',
            'email',
            'pengajar',
            'nama_gurupiket',
            'status',
            'tanggal',
            [
                'label' => 'Aksi',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Setuju', ['setuju', 'id' => $model->id], [
                        'class' => 'btn btn-success',
                        'data' => [
                            'confirm' => Yii::t('app', 'Apakah Anda yakin ingin menyetujui ini?'),
                            'method' => 'post',
                        ],
                    ]) . ' ' .
                    Html::a('Tolak', ['tolak', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Apakah Anda yakin ingin menolak ini?'),
                            'method' => 'post',
                        ],
                    ]);
                },
            ],
            
        ],
    ]) ?>

</div>
