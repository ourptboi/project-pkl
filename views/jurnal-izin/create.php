<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\DashboardAsset;
use yii\helpers\ArrayHelper;

DashboardAsset::register($this);

/**  @var yii\web\View $this */
/** @var app\models\JurnalIzin $model */
/** @var yii\widgets\ActiveForm $form */

$this->context->layout = 'main-no-layout\\\\\\\\';
?>


<div class="jurnal-izin-form"></div>
    <div class="row justify-content-center">
        <div class="col-lg-6">

                <div class="form-container" style="margin-top: 50px; height: 1220px;">
                
                <h1 style="text-align: center; font-weight: bold; color: black;"><?= Html::encode(Yii::t('app', 'Form Jurnal Izin')) ?></h1>

                <hr style="border: 2px solid #333;">

                <p style="text-align: center; color: #333; font-size: 17px;">
                    Untuk keperluan kalian ketika ada kegiatan di luar sekolah sehingga harus meninggalkan kegiatan belajar di kelas
                </p>


                <?php $form = ActiveForm::begin(); ?>
                <?= $this->render('_form', [
                    'model' => $model,
                    'listKeahlian' => $listKeahlian,
                    'listJam' => $listJam,
                    'listGuruPiket' => $listGuruPiket,
                ]) ?>
                   

                <?php ActiveForm::end(); ?>
            </div>
            <style>
                .form-container {
                    border: 1px solid #ccc; /* Atur gaya border sesuai kebutuhan */
                    padding: 20px; /* Atur padding sesuai kebutuhan */
                    border-radius: 5px; /* Untuk sudut yang lebih lembut jika diperlukan */
                    background-color: white;
                }
                body{
                    background-color: #4e73df;
                    
                }
                .help-block {
                    color: #FF0000; 
                }
                ::-webkit-scrollbar {
                    width: 0.0em; /* Lebar scrollbar */
                }

                ::-webkit-scrollbar-track {
                    background-color: transparent; /* Warna latar belakang track */
                }

                ::-webkit-scrollbar-thumb {
                    background-color: transparent; /* Warna thumb scrollbar */
                }

                * {
                    scrollbar-width: none; /* Untuk Firefox versi terbaru */
                }

                *::-ms-scrollbar {
                    width: 0.0em; /* Lebar scrollbar */
                }

                *::-ms-scrollbar-thumb {
                    background-color: transparent; /* Warna thumb scrollbar */
                }
                    </style>
                </div>
            </div>
        </div>