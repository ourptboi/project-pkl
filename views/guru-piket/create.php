<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\GuruPiket $model */

$this->title = Yii::t('app', 'Create Guru Piket');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Guru Pikets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guru-piket-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
