<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\GuruPiket $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="guru-piket-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nip')->textInput() ?>

    <?= $form->field($model, 'nama_gurupiket')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenkel')->dropDownList([ 'laki - laki' => 'Laki - laki', 'perempuan' => 'Perempuan', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
