<?php

use app\models\Jam;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'List Jam Pelajaran');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800"><?= Html::encode($this->title) ?></h1>


    <p>
        <?= Html::a(Yii::t('app', 'Create Jam'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<div class="card shadow mb-4">
    <div class="card-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'id', // Ganti 'id' dengan atribut yang sesuai dari model Anda
                    'label' => 'No', // Label untuk kolom
                    'format' => 'text', // Format sebagai teks
                    'value' => function ($model, $key, $index, $column) {
                        return $index + 1;
                    }
                ],

                'id',
                'jam_pelajaran',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Jam $model, $key, $index, $column) {
                        return Url::toRoute([$action, 'id' => $model->id]);
                    }
                ],
            ],
        ]); ?>


    </div>
