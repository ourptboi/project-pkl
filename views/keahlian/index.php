<?php

use app\models\Keahlian;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Keahlian');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Keahlian'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="card shadow mb-4">
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'columns' => [
                    [
                        'attribute' => 'id', // Ganti 'id' dengan atribut yang sesuai dari model Anda
                        'label' => 'No', // Label untuk kolom
                        'format' => 'text', // Format sebagai teks
                    ],


                    'id',
                    'keahlian',
                    [
                        'class' => ActionColumn::className(),
                        'header' => 'Aksi',
                        'urlCreator' => function ($action, Keahlian $model, $key, $index, $column) {
                            return Url::toRoute([$action, 'id' => $model->id]);
                        }
                    ],
                ],
            ]); ?>


        </div>