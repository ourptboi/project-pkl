<?php

/** @var yii\web\View $this */

use yii\helpers\Html;
use yii\helpers\Url;

 $this->title = 'About Us';
$this->params['breadcrumbs'][] = $this->title;
?>

<!DOCTYPE html>
<html>
    <head>
        <title>halaman about</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>

    <div class="site-about">
        <h1><?= Html::encode($this->title) ?></h1>
        <p>This is About page</p>
    </div>
    <div class="container">
        <section class="about-image">
            <img src="<?= Url::base(true)?>/image/about.jpg">
            <div class="content">
                <h5>SMK N 1 Bantul</h5>
                <p>awertyhujikolpolkiujhgfdswdefrgthyjuki7,6munbyetvrcewert5y6u7i8ko9l7iuyhgtvrfedxwsedrfgtyhujikolkiujhybgtvfc</p>
                <button class="read-more">more >></button>
            </div>
        </section>
    </div>
</html>