<?php

use app\models\History;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$dataProvider = new ActiveDataProvider([
    'query' => History::find()->where(['status' => ['setuju', 'ditolak']]),
    // ...
]);

$this->title = Yii::t('app', 'History');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
document.getElementById('print-button').addEventListener('click', function () {
    window.print();
});
JS;
$this->registerJs($js);
?>

<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Print', '#', ['class' => 'btn btn-primary', 'id' => 'print-button']) ?>
    </p>


    <div class="card shadow mb-4">
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    'nama',
                    //'kelas',
                    //'keahlian',
                    [
                        'attribute' => 'sekolah',
                        'value' => 'Sekolah',
                    ],
                    'jam_mulai',
                    'jam_kembali',
                    'keterangan',
                    //'tlp',
                    'pengajar',
                    'nama_gurupiket',
                    //'status',
                    'tanggal',
                ],
            ]); ?>
        </div>
    </div>
</div>