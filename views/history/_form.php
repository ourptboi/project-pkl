<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\History $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="history-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kelas')->dropDownList([ 'X' => 'X', 'XI' => 'XI', 'XII' => 'XII', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'keahlian')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jam_mulai')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jam_kembali')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tlp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pengajar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_gurupiket')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'setuju' => 'Setuju', 'ditolak' => 'Ditolak', 'belum diproses' => 'Belum diproses', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'tanggal')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
