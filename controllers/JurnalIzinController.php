<?php

namespace app\controllers;

use app\models\JurnalIzin;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Keahlian;
use app\models\Jam;
use app\models\GuruPiket;
use Yii;

/**
 * JurnalIzinController implements the CRUD actions for JurnalIzin model.
 */
class JurnalIzinController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all JurnalIzin models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => JurnalIzin::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JurnalIzin model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new JurnalIzin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
{
    $model = new JurnalIzin();
    $listKeahlian = Keahlian::find()->all();
    $listJam = Jam::find()->all();
    $listGuruPiket = GuruPiket::find()->all();

    // Set nilai default untuk status
    $model->status = 'belum diproses';

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
        Yii::$app->session->setFlash('success', 'Data di approve');
        return $this->redirect(['view', 'id' => $model->id]);
    }

    return $this->render('create', [
        'model' => $model,
        'listKeahlian' => $listKeahlian,
        'listJam' => $listJam,
        'listGuruPiket' => $listGuruPiket,
    ]);
}


    /**
     * Updates an existing JurnalIzin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $listKeahlian = Keahlian::find()->all();
        $listJam = Jam::find()->all();
        $listGuruPiket = GuruPiket::find()->all();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'listKeahlian' => $listKeahlian,
            'listJam' => $listJam,
            'listGuruPiket' => $listGuruPiket,
        ]);
    }

    /**
     * Deletes an existing JurnalIzin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JurnalIzin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return JurnalIzin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JurnalIzin::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionSetuju($id)
    {
        $model = $this->findModel($id);
        $model->status = 'setuju'; // Mengubah status menjadi "setuju"

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Data berhasil disetujui.');
        } else {
            Yii::$app->session->setFlash('error', 'Gagal menyimpan data.');
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    public function actionTolak($id)
    {
        $model = $this->findModel($id);
        $model->status = 'ditolak'; // Mengubah status menjadi "ditolak"

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Data berhasil ditolak.');
        } else {
            Yii::$app->session->setFlash('error', 'Gagal menyimpan data.');
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }
}
